const express = require('express');
const gmapsClient = require('../gmaps');
const redisClient = require('../redis');
const Alumnos = require('../bd/modelos/alumnosModelo');
const rutas = express.Router();

rutas.get('/alumnos', (req, res, next) => {
    Alumnos.getAll().then(alumnos => res.send(alumnos));
});
rutas.get('/alumno/:id', (req, res, next) => {
    const idAlumno = req.params.id;
    if (isNaN(idAlumno)) return next(new Error('La id no es numérica'));
    Alumnos.getById(idAlumno).then(alumno => res.send(alumno));
});
rutas.post('/alumno', (req, res, next) => {
    const nuevoAlumno = req.body;
    formaYCreaAlumno(nuevoAlumno);
});

function formaYCreaAlumno(nuevoAlumno) {
    redisClient.hgetall('cp-' + nuevoAlumno.cp, (err, respuesta) => {
        if (err) return console.log("Error: " + err.message);
        if (!respuesta) {
            console.log("Cogiendo el código postal de GMaps");
            gmapsClient.getLocalidadProvincia(nuevoAlumno.cp)
                .then(datos => {
                    redisClient.hmset('cp-' + nuevoAlumno.cp, 'localidad', localidad, 'provincia', provincia);
                    Alumnos.crea(nuevoAlumno, localidad, provincia, res).then(alumno => {
                        console.log("Creado nuevo alumno", nuevoAlumno);
                        res.send("OK");
                    })
                })
                .catch(err => console.log("Error: " + err.message));
        } else {
            const localidad = respuesta.localidad;
            const provincia = respuesta.provincia;
            console.log("Cogiendo el código postal de REDIS");
            Alumnos.crea(nuevoAlumno, localidad, provincia, res).then(alumno => {
                console.log("Creado nuevo alumno", nuevoAlumno);
                res.send("OK");
            })
        }
    });
}

module.exports = rutas;