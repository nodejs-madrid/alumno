const debug = require('debug');
const chalk = require('chalk');

function errorMsj(msj) {
    return chalk.red('[Error] ') + msj;
}

module.exports = function (namespace) {
    dbg = debug(namespace);
    return {
        mensaje: function (mensaje) {
            return dbg(mensaje);
        },
        error: function (mensaje) {
            return dbg(errorMsj(mensaje));
        }
    };
}