const debug = require('../debug')('servidor');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];
const rutas = require('../rutas');
const app = express();
const server = http.createServer(app);

if (env == 'development') {
    app.use(logger('dev'));
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/curso-api', rutas);
app.use((req, res, next) => {
    debug.error('No encontrado');
    res.status(404).send('No encontrado');
});
app.use((err, req, res, next) => {
    if (err) {
        debug.error(err.message);
        res.status(500).send('Error: ' + err.message);
    }
});

module.exports = server;