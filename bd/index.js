const debug = require('debug')('api:BD');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];
const Sequelize = require('sequelize');

const sequelize = new Sequelize(
    config.bd.nombre,
    config.bd.user,
    config.bd.pwd, {
        host: config.bd.host,
        logger: mensaje => debug(mensaje),
        operatorsAliases: false,
        dialect: 'mysql',
        dialectOptions: { decimalNumbers: true }
    }
);

module.exports = sequelize;