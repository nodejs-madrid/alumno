const Sequelize = require('sequelize');
const sequelize = require('../');

const Alumnos = sequelize.define('alumnos', {
    nombre: {
        type: Sequelize.STRING
    },
    apellidos: {
        type: Sequelize.STRING
    },
    cp: {
        type: Sequelize.STRING,
        allowNull: false
    },
    localidad: {
        type: Sequelize.STRING
    },
    provincia: {
        type: Sequelize.STRING
    }
}, {
        freezeTableName: true,
        timestamps: false
    });

module.exports = {
    getAll: function () {
        return Alumnos.findAll({ raw: true });
    },
    getPorId: function (id) {
        return Alumnos.findById(idAlumno, { raw: true });
    },
    crea: function (nuevoAlumno, localidad, provincia, res) {
        nuevoAlumno.localidad = localidad;
        nuevoAlumno.provincia = provincia;
        return Alumnos.create(nuevoAlumno);
    }
}