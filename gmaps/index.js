const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];
const gmaps = require('@google/maps');

const googleMapsClient = gmaps.createClient({
    key: config.gMapsKey
});

function getLocalidadProvincia(cp) {
    googleMapsClient.geocode({
        address: cp + ' España',
        language: 'es'
    }, function (err, response) {
        return new Promise((resolve, reject) => {
            if (err) return reject(err);

            const localidad = response.json.results[0].address_components[1].long_name;
            const provincia = response.json.results[0].address_components[2].long_name;
            resolve({ localidad, provincia });
        })
    });
}

module.exports = { getLocalidadProvincia };
