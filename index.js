const debug = require('debug')('api:main');
const env = process.env.NODE_ENV || 'development';
const config = require('./config/config.json')[env];
const sequelize = require('./bd');
const server = require('./server');
const serverWWW = require('./www');
const redisClient = require('./redis');

sequelize.authenticate().then(() => {
    debug('Conectado a la BD');
}).catch(err => {
    debug('Error al conectar a la BD: ' + err.message);
});

redisClient.on('connect', () => {
    debug('Conectado a Redis');
}).on('error', err => {
    debug('Error al conectar a Redis: ' + err.message);
});

server.listen(config.puertoAPI, () => {
    debug(`Servidor API corriendo en el puerto ${config.puertoAPI}`);
});