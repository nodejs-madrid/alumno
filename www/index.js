const axios = require('axios');
const debug = require('debug')('api:www');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];
const path = require('path');
const http = require('http');
const express = require('express');
const logger = require('morgan');
const hbs = require('express-handlebars');
const Alumnos = require('../bd/modelos/alumnosModelo');

const app = express();

const server = http.createServer(app);

app.engine('hbs', hbs());
app.set('view engine', 'hbs');
app.set('views', path.resolve(__dirname, 'web', 'vistas'));

app.use(logger('dev'));
app.use(express.static(path.resolve(__dirname, 'web', 'public')));

app.get('/', async (req, res) => {
    //let alumnos = await axios.get('http://localhost:' + config.puertoAPI + '/curso-api/alumnos');
    let alumnos = await Alumnos.getAll();
    res.render('index', { alumnos });
});

server.listen(config.puertoWWW, () => {
    debug(`Servidor WWW corriendo en el puerto ${config.puertoWWW}`);
})

exports.module = server;